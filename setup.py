#!/usr/bin/env python
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=79 cc=+1:

import os
import setuptools

script_dirname = os.path.dirname( __file__ )
script_dirname_abs = os.path.abspath( script_dirname )
script_basename = os.path.basename( __file__ )

description_file_name = os.path.join( script_dirname, "DESCRIPTION.rst" )
with open( description_file_name, "r" ) as description_file:
    long_description = description_file.read()

module_name = "ichuru"

## TODO

setuptools.setup(
    name = module_name,

    author = "Iwan Aucamp",
    author_email = "aucampia@gmail.com",

    maintainer = "Iwan Aucamp",
    maintainer_email = "aucampia@gmail.com",

    url = "https://gitlab.com/iwan.aucamp/" + module_name,

    description = "TODO",

    long_description = long_description,

    download_url = "https://gitlab.com/iwan.aucamp/" + module_name,

    classifiers=[
            "Development Status :: 3 - Alpha",
    ],

#   platforms = [ "", "", "", ]

#    license = "TODO",

    keywords="key word",

    package_dir = { "" : "src", },

    packages = [
        module_name,
    ],

    entry_points = {
        "console_scripts": [
            module_name + " = " + module_name + ".cli:main",
        ],
    },

    setup_requires=[ "vcversioner", ],

    vcversioner = {
        "version_file": "version.txt",
        "version_module_paths": [ "src/" + module_name + "/__version__.py" ],
    },
)
