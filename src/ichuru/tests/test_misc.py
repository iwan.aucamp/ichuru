#!/usr/bin/env python2
# vim: set ft=python sts=4 ts=4 sw=4 expandtab tw=79 cc=+1 fo-=t:

import unittest
import ichuru

class TestFunctions( unittest.TestCase ):
    def setUp( self ):
        self.longMessage = True
        None

    def tearDown( self ):
        None

    def test_sanitize( self ):
        input = [
            [ 'mongo::BtreeKeyGeneratorV1::getKeysImpl(std::vector<char const*, std::allocator<char const*> >, std::vector<mongo::BSONElement, std::allocator<mongo::BSONElement> >, mongo::BSONObj const&, std::set<mongo::BSONObj, mongo::BSONObjCmp, std::allocator<mongo::BSONObj> >*) const', 'mongo::BtreeKeyGeneratorV1::getKeysImpl' ],
        [ '', '' ],
        ]
        self.assertEqual( ichuru.clean_frame( input ),
            "mongo::BtreeKeyGeneratorV1::getKeysImpl" )
