#!/usr/bin/env python2
# vim: set ft=python sts=4 ts=4 sw=4 expandtab tw=79 cc=+1 fo-=t:

import sys
import os
import argparse
import logging
import ichuru
import string
import ctypes
import collections
import re
import json
import enum
import operator

@enum.unique
class OutputMode( enum.IntEnum ):
    flame = 0
    dot = 1
    table = 2

@enum.unique
class OutputValues( enum.IntEnum ):
    calls = 0
    stacks = 1
    tops = 2
    frames = 3

@enum.unique
class OutputQuoting( enum.IntEnum ):
    comment = 0

class StoreEnum( argparse.Action ):
    def __init__( self, **kwargs ):
        if kwargs.get( "nargs", None ) is not None:
            raise ValueError("nargs not allowed")
        self.etype = kwargs[ "etype" ]
        del kwargs[ "etype" ]
        kwargs[ "choices" ] = tuple( i.name for i in OutputMode )
        super( StoreEnum, self ).__init__( **kwargs )
    def __call__( self, parser, namespace, values, option_string = None ):
        setattr( namespace, self.dest, self.etype[ values ] )

def clean_frame_0( function ):
    result = collections.deque()
    ignore = 0
    skip = 0
    if function.endswith( " const" ):
        skip += 6
    for index in range( len( function ) - 1 - skip, -1, -1 ):
        char = function[ index ]
        if char in ( ")", ">" ):
            ignore += 1
            continue
        if ignore and char in ( "(", "<" ):
            ignore -= 1
            continue
        if not ignore:
            if char == " ":
                break
            result.appendleft( char )
    return string.join( result, "" )

def clean_frame_1( function ):
    result = ctypes.create_string_buffer( len( function ) )
    result_end = len( function ) - 1
    result_pointer = result_end
    ignore = 0
    skip = 0
    if function.endswith( " const" ):
        skip += 6
    for index in range( len( function ) - 1 - skip, -1, -1 ):
        char = function[ index ]
        if char in ( ")", ">" ):
            ignore += 1
            continue
        if ignore and char in ( "(", "<" ):
            ignore -= 1
            continue
        if not ignore:
            if char == " ":
                break
            result[ result_pointer ] = char
            result_pointer -= 1
    return result[result_pointer+1:]

def clean_frame_2( function ):
    result = bytearray( len( function ) )
    result_end = len( function ) - 1
    result_pointer = result_end
    ignore = 0
    skip = 0
    if function.endswith( " const" ):
        skip += 6
    for index in range( len( function ) - 1 - skip, -1, -1 ):
        char = function[ index ]
        if char in ( ")", ">" ):
            ignore += 1
            continue
        if ignore and char in ( "(", "<" ):
            ignore -= 1
            continue
        if not ignore:
            if char == " ":
                break
            result[ result_pointer ] = char
            result_pointer -= 1
    return str(result[result_pointer+1:])

clean_frame = clean_frame_2

def phelp( iterations, function, *args, **kwargs ):
    for interation in range( 0, iterations ):
        function( *args, **kwargs )

class Aggregator( object ):

    top_ignore = set( [
            "pthread_cond_timedwait",
            "pthread_cond_wait",
            "__lll_lock_wait",
            "_L_lock_",
            "pthread_mutex_lock",
            "recv",
            "select",
            "sigwait",
            "sem_wait",
            "??",
            "nanosleep",
            "runtime.futex",
            "runtime.epollwait",
            "ioctl",
            "pollsys",
            "sigtimedwait",
            "_write",
            "lwp_park",
            "poll",
            "__posix_sigwait",
            "cond_wait_queue",
            "_cond_wait",
            "cond_wait",
            "cond_wait_common",
            "_cond_timedwait",
            "cond_timedwait",
        ] )

    thread_regex = re.compile(
        #"^Thread ([0-9]+) [(]Thread 0x([A-Fa-f0-9]+) [(]LWP ([0-9]+)[)][)]:$" )
        "^(Thread ([0-9]+) [(]Thread 0x([A-Fa-f0-9]+) [(]LWP ([0-9]+)[)][)]:|-+  lwp# ([0-9]+) / thread# ([0-9]+)  -+)$" )
    frame_regex = re.compile(
        #"^#([0-9]+)\s+(?:0x([0-9A-Fa-f]+) in |)(.*) [(][^)]*[)](?: from (.*)| at (.*)|)$" )
        #"^#(?:[0-9]+)\s+(?:0x(?:[0-9A-Fa-f]+) in |?:)(.*) [(][^)]*[)](?: from (?:.*)| at (?:.*)|)$" )
        "^(?:#(?:[0-9]+)\s+(?:0x(?:[0-9A-Fa-f]+) in |)| [0-9A-Fa-f]+ )(.*\S)\s+[(][^)]*[)](?: from (?:.*)| at (?:.*)| [+] .*|)$" )
    proc_regex = re.compile(
        "^([0-9]+:)\s+(.*)$" )

    def __init__( self ):
        self.stacks = {}
        self.calls = {}
        self.tops = {}
        self.frames = {}

    def process_stack( self, stack ):
        stack_tuple = tuple( stack )
        logging.debug( "stack_tuple = %s", stack_tuple )

        if stack_tuple in self.stacks:
            self.stacks[ stack_tuple ] += 1
        else:
            self.stacks[ stack_tuple ] = 1

        prev = None
        for frame in stack:
            if frame in self.frames:
                self.frames[ frame ] += 1
            else:
                self.frames[ frame ] = 1
            if prev is not None:
                call = ( prev, frame )
                if call in self.calls:
                    self.calls[ call ] += 1
                else:
                    self.calls[ call ] = 1
            prev = frame
        top = None
        for frame_index in range( len( stack ) - 1, -1, -1 ):
            frame = stack[ frame_index ]
            if frame in self.top_ignore:
                None
            elif any( frame.startswith( prefix )
                for prefix in self.top_ignore ):
                None
            else:
                top = frame
                break
        if top in self.tops:
            self.tops[ top ] += 1
        else:
            self.tops[ top ] = 1

    def process_file( self, input_file ):
        stack = None
        for line in input_file.read().splitlines():
            logging.debug( "line = %s", line )
            frame_match = Aggregator.frame_regex.match( line )
            thread_match = Aggregator.thread_regex.match( line ) \
                if not frame_match else None
            proc_match = Aggregator.proc_regex.match( line ) \
                if not thread_match and not frame_match else None
            if thread_match:
                logging.debug( "thread_match.groups() = %s",
                    thread_match.groups() )
                if stack is not None:
                    self.process_stack( stack )
                stack = collections.deque()
            elif proc_match:
                logging.debug( "proc_match.groups() = %s",
                    proc_match.groups() )
            elif frame_match:
                logging.debug( "frame_match.groups() = %s",
                    frame_match.groups() )
                clean = clean_frame( frame_match.group( 1 ) )
                if clean != "??":
                    stack.appendleft( clean )
                    logging.debug( "clean = %s", clean )
            else:
                raise ValueError( "bad input line {}".format( line ) )
        if stack is not None:
            self.process_stack( stack )

    def process_filename( self, input_filename ):
        input_file = None
        try:
            if input_filename != "-":
                input_file = open( input_filename, "rb" )
            else:
                input_file = sys.stdin
            self.process_file( input_file )
        finally:
            if input_file is not None and input_file is not sys.stdin:
                input_file.close()

    def get_calls( self ):
        result = {}
        for stack, count in self.stacks.iteritems():
            prev = None
            for frame in stack:
                if prev is not None:
                    call = ( prev, frame )
                    logging.debug( "call = %s", call )
                    if call in result:
                        result[ call ] += count
                    else:
                        result[ call ] = count
                prev = frame
        return result

class FormatTable( object ):

    format_start = ""
    format_end = ""
    comment_start = ""
    comment_end = ""

    @classmethod
    def format( cls, values, indent = "", sort = False, reverse = False ):
        if sort:
            values = collections.OrderedDict(
                sorted( values.items(),
                    key = operator.itemgetter( 1 ), reverse = reverse ) )
        for item, label in values.iteritems():
            first = True
            sys.stdout.write( "{: >9}\t".format( label ) )
            cls.format_item( item )
            sys.stdout.write( "\n" )

    @classmethod
    def format_item( cls, item ):
        if type( item ) is tuple:
            first = True
            for part in item:
                if first: first = False
                else: sys.stdout.write( "->" )
                sys.stdout.write( part )
        else:
            sys.stdout.write( str( item ) )

    @classmethod
    def comment( cls, *args, **kwargs ):
        cls.format( *args, **kwargs )

    @classmethod
    def comment_item( cls, *args, **kwargs ):
        cls.format_item( *args, **kwargs )

class FormatDot( object ):

    format_start = "digraph {\n"
    format_end = "}\n"
    comment_start = "/*\n"
    comment_end = "*/\n"

    @classmethod
    def format( cls, values, indent = "", sort = False, reverse = False ):
        if sort:
            values = collections.OrderedDict(
                sorted( values.items(),
                    key = operator.itemgetter( 1 ), reverse = reverse ) )
        for item, label in values.iteritems():
            sys.stdout.write( indent )
            cls.format_item( item )
            sys.stdout.write( " [ label = \"{}\" ]\n".format( label ) )

    @classmethod
    def format_item( cls, item ):
        if type( item ) is tuple:
            first = True
            for part in item:
                if first: first = False
                else: sys.stdout.write( " -> " )
                sys.stdout.write( "\"" )
                sys.stdout.write( part )
                sys.stdout.write( "\"" )
        else:
            sys.stdout.write( str( item ) )

    @classmethod
    def comment( cls, *args, **kwargs ):
        FormatTable.format( *args, **kwargs )

    @classmethod
    def comment_item( cls, *args, **kwargs ):
        FormatTable.format_item( *args, **kwargs )

class FormatFlame( object ):

    format_start = ""
    format_end = ""
    comment_start = ""
    comment_end = ""

    @classmethod
    def format( cls, values, indent = "", sort = False, reverse = False ):
        if sort:
            values = collections.OrderedDict(
                sorted( values.items(),
                    key = operator.itemgetter( 1 ), reverse = reverse ) )
        for item, label in values.iteritems():
            first = True
            cls.format_item( item )
            sys.stdout.write( " {}".format( label ) )
            sys.stdout.write( "\n" )

    @classmethod
    def format_item( cls, item ):
        if type( item ) is tuple:
            first = True
            for part in item:
                if first: first = False
                else: sys.stdout.write( ";" )
                sys.stdout.write( part )
        else:
            sys.stdout.write( str( item ) )

    @classmethod
    def comment( cls, *args, **kwargs ):
        cls.format( *args, **kwargs )

    @classmethod
    def comment_item( cls, *args, **kwargs ):
        cls.format_item( *args, **kwargs )


class Cli( object ):
    def __init__( self, prog ):
        assert type( prog ) is str

        version_string_proto = "%(prog)s {version:}-{revision:}-000"
        version_string = version_string_proto.format(
            version = ichuru.__version__,
            revision = ichuru.__revision__ )

        description = version_string

        epilog_proto = """
examples:

\tichuru mongod.rs-dm.2016022403????.*.pstack --mode flame --data stacks | flamegraph.pl > flamestacks.svg
\tichuru mongod.rs-dm.2016022403????.*.pstack --mode dot --data calls | dot -Tsvg -o callgraph.svg /dev/stdin
\tichuru mongod.rs-dm.2016022403????.*.pstack --mode dot --data comment:tops --data calls | dot -Tsvg -o callgraph.svg /dev/stdin
\tichuru mongod.rs-dm.2016022403????.*.pstack --mode table --data tops --data calls

ls -d hxcdbs-???.mobilink.pk/pstacks-000 | xargs -n1 bash -c 'tn=${1//\//-}; ichuru --mode dot --data comment:tops --data calls "${1}"/*.pstack > "${1}"/${tn}.aggr.dot' /dev/null
ls -d hxcdbs-???.mobilink.pk/pstacks-000/*.pstack | xargs -n1 bash -c 'ichuru --mode dot --data comment:tops --data calls "${1}" > "${1}".dot' /dev/null
find . -type f -name '*.dot' | xargs -t -I{} dot -Tsvg -o {}.svg {}
"""

        epilog = epilog_proto;

        self.argument_parser = argparse.ArgumentParser(
            description = description, epilog = epilog,
            formatter_class = argparse.RawDescriptionHelpFormatter,
            prog = prog, add_help = False, )

        self.argument_parser.add_argument( "-v", "--verbose",
            help = "increase verbosity",
            action = "count", dest = "verbosity", )

        self.argument_parser.add_argument( "-h", "--help",
            help = "show this help message and exit",
            action = "help", )

        self.argument_parser.add_argument( "--version",
            help = "show program's version number and exit",
            action = "version", version=( version_string ), )

        self.argument_parser.add_argument( "--mode",
            help = "sets output mode", action = StoreEnum, etype = OutputMode,
            required = True, dest = "output_mode" )

        self.argument_parser.add_argument( "--data",
            help = "select data to output [comment:]{{{}}}".format(
                ",".join( i.name for i in list( OutputValues ) ) ),
            action = "append", type = str,
            default = None, dest = "data"
            )

        self.argument_parser.add_argument( "input_filenames",
            help = "input file",
            action = "store", type = str, nargs = "*",
            default = [ "-" ], metavar = "FILE", )

    def main( self, args ):

        assert type( args ) is list

        self.arguments = self.argument_parser.parse_args( args = args )

        # -v gets info, more -v gets debug and nothing else ...
        if self.arguments.verbosity > 0:
            level = 20 - ( min( 1, self.arguments.verbosity -1 ) * 10 );
            msgformat = '%(asctime)s %(module)s:%(lineno)d %(message)s';
            dateformat = '%Y-%m-%dT%H:%M:%S';
            logging.basicConfig(
                level = level, format = msgformat, datefmt=dateformat );

            logging.info( "logging.level = %s", level )

        logging.info( "output_mode = %s",
            self.arguments.output_mode )

        logging.info( "data = %s",
            self.arguments.data )

        #"comment:tops", "calls"

        logging.info( "input_filenames = %s",
            self.arguments.input_filenames )

        aggregator = Aggregator()

        for input_filename in self.arguments.input_filenames:
            aggregator.process_filename( input_filename )

        format_class = None
        if self.arguments.output_mode is OutputMode.dot:
            format_class = FormatDot
        elif self.arguments.output_mode is OutputMode.table:
            format_class = FormatTable
        elif self.arguments.output_mode is OutputMode.flame:
            format_class = FormatFlame

        sys.stdout.write( format_class.format_start );
        for data_item in self.arguments.data:
            comment = False
            if data_item.startswith( "comment:" ):
                comment = True
                data_item = data_item[ len( "comment:" ) : ]
            if comment:
                sys.stdout.write( format_class.comment_start );
            output_value = OutputValues[ data_item ]
            data = None
            if output_value == OutputValues.calls:
                data = aggregator.calls
            elif output_value == OutputValues.stacks:
                data = aggregator.stacks
            elif output_value == OutputValues.tops:
                data = aggregator.tops
            elif output_value == OutputValues.frames:
                data = aggregator.frames
            if comment:
                format_class.comment( data, sort = True )
            else:
                format_class.format( data, sort = True )
            if comment:
                sys.stdout.write( format_class.comment_end );
        sys.stdout.write( format_class.format_end );
#        if self.arguments.output_mode is OutputMode.graphviz_calls:
#            format_table( aggregator.tops, sort = True )
#            format_dot( aggregator.calls, sort = True )
#        elif self.arguments.output_mode is OutputMode.graphviz_stacks:
#            format_dot( aggregator.stacks, sort = True )
#        else:
#            None

    @classmethod
    def run( cls, prog, args ):

        assert type( prog ) is str
        assert type( args ) is list

        instance = cls( prog )
        return instance.main( args )


def main( prog = None, args = None ):
    if prog is None:
        prog = os.path.basename( sys.argv[ 0 ] )
    if args is None:
        args = sys.argv[ 1 : ]
    return_value = Cli.run( prog, args )
    if return_value != os.EX_OK: sys.exit( return_value )

if __name__ == "__main__":
    main()
