import ichuru
import cProfile

input = "mongo::WriteBatchExecutor::bulkExecute(mongo::BatchedCommandRequest const&, mongo::WriteConcernOptions const&, std::vector<mongo::BatchedUpsertDetail*, std::allocator<mongo::BatchedUpsertDetail*> >*, std::vector<mongo::WriteErrorDetail*, std::allocator<mongo::WriteErrorDetail*> >*)"

print( ichuru.sanitize_function_0( input ) )
print( ichuru.sanitize_function_1( input ) )
print( ichuru.sanitize_function_2( input ) )


cProfile.run( 'ichuru.phelp( 10000, ichuru.sanitize_function_0, input )', sort="cumtime" )
cProfile.run( 'ichuru.phelp( 10000, ichuru.sanitize_function_1, input )', sort="cumtime" )
cProfile.run( 'ichuru.phelp( 10000, ichuru.sanitize_function_2, input )', sort="cumtime" )

# python2 -m cProfile -s cumtime /home/iwana/.local/bin/ichuru mongod.rs-dm.2016022403????.34561.pstack
